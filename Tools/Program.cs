﻿using System;
using System.Net;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;
using System.Net.Http;

namespace Tools
{
    public class Tools
    {
        public static string pathS3DaYuan = @"\\52.15.86.18\storage-for-station\DaYuanCbm\log\";
        public static string pathS3TaiChung = @"\\52.15.86.18\storage-for-station\TaiChungCbm\log\";
        public static string fromEmail = @"support@light-year.tech";
        public static string emailPassword = "zcjnzdigyfdacgxh";
        public static string destinationEmail = @"angus.hsieh@junfu.asia";
        public static string destinationEmail2 = @"arthur.hsu@junfu.asia";
        public static string destinationEmail3 = @"sali@light-year.tech";
        public static string destExcelFolder = @"D:\CbmAbnormalRate\";
        public static string excelName = "";
        public static string AWSDataBaseConnectionString = @"Initial Catalog=JunfuReal;server=172.30.1.34,1433; Initial Catalog = JunFuReal; Persist Security Info = True;User Id=lyAdmin;Password=P@ssw0rd!!!";
        public static void GetDBColumns(string FromDB, string FromTable, string ToDB, string ToTable)   //閹割版
        {
            string result = "";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = $"SELECT COLUMN_NAME,ORDINAL_POSITION,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{FromTable}'";
                cmd.Connection = new SqlConnection(FromDB);
                cmd.Connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result += reader["COLUMN_NAME"].ToString() + ",";
                }
                cmd.Connection.Close();
            }
        }

        public static void MainCbm(string station1 = "大園倉", string station2 = "台中倉")       //29:大園,49:台中
        {
            string dataName = destExcelFolder + "Cbm" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            if (!File.Exists(dataName))
            {
                XLWorkbook wb = new XLWorkbook();
                var DaYuanSheet = wb.Worksheets.Add(station1);
                var TaiChungSheet = wb.Worksheets.Add(station2);
                wb.SaveAs(dataName);
            };
            excelName = dataName;
            try
            {


                Console.WriteLine("大園開始抓取");
                CbmCalculatorEntity daYuanResult = CbmCalculator(pathS3DaYuan);
                Console.WriteLine("大園開始建置");
                ExcelInsertor(daYuanResult, station1, dataName);
                Console.WriteLine("台中開始抓取");
                CbmCalculatorEntity TaiChungResult = CbmCalculator(pathS3TaiChung);
                Console.WriteLine("台中開始建置");
                ExcelInsertor(TaiChungResult, station2, dataName);
                Console.WriteLine("開始寄信");
                EMailPostMan();
                Console.WriteLine("結束");
            }
            catch (Exception e)
            {
                Console.WriteLine("Message: " + e.Message);
                Console.WriteLine("Source: " + e.Source);
                Console.WriteLine("StackTrace: " + e.StackTrace);
            }
        }

        public static CbmCalculatorEntity CbmCalculator(string pathS3)
        {
            List<DailyCauculator> dailyResult = new List<DailyCauculator>();
            List<MonthlyCalculator> monthlyResult = new List<MonthlyCalculator>();

            string[] fileArray = Directory.GetFiles(pathS3, "*.csv");

            for (var i = 0; i < fileArray.Length; i++)
            {
                string[] dataNameSplit = fileArray[i].Split(@"\");
                string dataNameDate = dataNameSplit[dataNameSplit.Length - 1].Replace(".csv", "").Substring(0, 8);
                int index = dailyResult.FindIndex(d => d.Date == dataNameDate);

                if (index == -1)
                {
                    DailyCauculator dc = new DailyCauculator()
                    {
                        Date = dataNameDate,
                        SuccessTotal = 0,
                        FailTotal = 0,
                        Total = 0,
                        Percent = ""
                    };

                    dailyResult.Add(dc);
                };

                index = dailyResult.FindIndex(d => d.Date == dataNameDate);     //新增後取得index

                var content = File.ReadAllLines(fileArray[i]);
                for (var j = 0; j < content.Length; j++)
                {
                    string[] cell = content[j].Split(",");
                    if (cell.Length < 6)
                    {

                    }
                    else if (cell[0] != "MULTRD" & cell[0] != "NOREAD" & cell[0] != "" & cell[7] != "Hand")
                    {
                        long checkNumber;
                        if (long.TryParse(cell[0], out checkNumber))
                        {
                            dailyResult[index].SuccessTotal += 1;
                            dailyResult[index].Total += 1;
                        }
                        else
                        {
                            dailyResult[index].FailTotal += 1;
                            dailyResult[index].Total += 1;
                        }
                    }
                    else if (cell[0] != "" & cell[7] != "Hand")
                    {
                        dailyResult[index].FailTotal += 1;
                        dailyResult[index].Total += 1;
                    }
                }
            }

            for (var i = 0; i < dailyResult.Count; i++)
            {
                if (dailyResult[i].Total != 0)
                {
                    dailyResult[i].Percent = (Math.Round((decimal)dailyResult[i].SuccessTotal / dailyResult[i].Total, 4) * 100).ToString() + "%";
                }
                else
                {
                    dailyResult[i].Percent = "/";
                }

                if (monthlyResult.FindIndex(x => x.Month == dailyResult[i].Date.Substring(0, 6)) == -1)
                {
                    MonthlyCalculator mc = new MonthlyCalculator()
                    {
                        Month = dailyResult[i].Date.Substring(0, 6),
                        SuccessTotal = 0,
                        FailTotal = 0,
                        Total = 0,
                        Days = 0
                    };

                    monthlyResult.Add(mc);
                }

                int indexMonth = monthlyResult.FindIndex(x => x.Month == dailyResult[i].Date.Substring(0, 6));

                monthlyResult[indexMonth].SuccessTotal += dailyResult[i].SuccessTotal;
                monthlyResult[indexMonth].FailTotal += dailyResult[i].FailTotal;
                monthlyResult[indexMonth].Total += dailyResult[i].Total;
                monthlyResult[indexMonth].Days += 1;
            }

            CbmCalculatorEntity result = new CbmCalculatorEntity()
            {
                DailyCauculatorArray = dailyResult.ToArray(),
                MonthlyCalculatorArray = monthlyResult.ToArray()
            };
            return result;
        }

        public static void ExcelInsertor(CbmCalculatorEntity cbmCalculatorEntity, string workSheet, string dataName)
        {
            var workBookOutPut = new XLWorkbook(dataName);
            var workSheetOutPut = workBookOutPut.Worksheet(workSheet);

            workSheetOutPut.Cell(1, 1).Value = workSheet;
            workSheetOutPut.Cell(1, 1).Style.Fill.BackgroundColor = XLColor.Yellow;

            workSheetOutPut.Cell(1, 2).Value = "成功筆數";
            workSheetOutPut.Cell(1, 3).Value = "異常件數";
            workSheetOutPut.Cell(1, 4).Value = "通過丈量機筆數";
            workSheetOutPut.Cell(1, 5).Value = "成功比率";

            for (var i = 1; i < 6; i++)
            {
                workSheetOutPut.Column(i).Width = 20;
            }

            for (var i = 0; i < cbmCalculatorEntity.DailyCauculatorArray.Length; i++)
            {
                workSheetOutPut.Cell(i + 2, 1).Value = cbmCalculatorEntity.DailyCauculatorArray[i].Date;
                workSheetOutPut.Cell(i + 2, 2).Value = cbmCalculatorEntity.DailyCauculatorArray[i].SuccessTotal;
                workSheetOutPut.Cell(i + 2, 3).Value = cbmCalculatorEntity.DailyCauculatorArray[i].FailTotal;
                workSheetOutPut.Cell(i + 2, 4).Value = cbmCalculatorEntity.DailyCauculatorArray[i].Total;
                workSheetOutPut.Cell(i + 2, 5).Value = cbmCalculatorEntity.DailyCauculatorArray[i].Percent;
            }

            workSheetOutPut.Cell(1, 7).Value = workSheet;
            workSheetOutPut.Cell(1, 7).Style.Fill.BackgroundColor = XLColor.Yellow;

            workSheetOutPut.Cell(1, 8).Value = "成功筆數";
            workSheetOutPut.Cell(1, 9).Value = "異常件數";
            workSheetOutPut.Cell(1, 10).Value = "天數";
            workSheetOutPut.Cell(1, 11).Value = "通過丈量機筆數";

            for (var i = 7; i < 12; i++)
            {
                workSheetOutPut.Column(i).Width = 20;
            }

            for (var i = 0; i < cbmCalculatorEntity.MonthlyCalculatorArray.Length; i++)
            {
                workSheetOutPut.Cell(i + 2, 7).Value = cbmCalculatorEntity.MonthlyCalculatorArray[i].Month;
                workSheetOutPut.Cell(i + 2, 8).Value = cbmCalculatorEntity.MonthlyCalculatorArray[i].SuccessTotal;
                workSheetOutPut.Cell(i + 2, 9).Value = cbmCalculatorEntity.MonthlyCalculatorArray[i].FailTotal;
                workSheetOutPut.Cell(i + 2, 10).Value = cbmCalculatorEntity.MonthlyCalculatorArray[i].Days;
                workSheetOutPut.Cell(i + 2, 11).Value = cbmCalculatorEntity.MonthlyCalculatorArray[i].Total;
            }

            workSheetOutPut.RowHeight = 15;
            workSheetOutPut.Style.Font.FontSize = 12;
            workSheetOutPut.Range(string.Format("E2:E{0}", cbmCalculatorEntity.DailyCauculatorArray.Length) + 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

            workBookOutPut.Save();
        }

        public static void EMailPostMan()           //請記得輸入Email帳號密碼
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(fromEmail);
                mail.To.Add(destinationEmail);
                mail.To.Add(destinationEmail2);
                mail.To.Add(destinationEmail3);
                mail.Subject = DateTime.Now.ToString("yyyyMMdd") + "附件為大園台中倉丈量機異常件數表";
                mail.Body = "附件為大園台中倉丈量機異常件數表";
                mail.IsBodyHtml = false;

                mail.Attachments.Add(new Attachment(excelName));
                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    smtp.Credentials = new NetworkCredential(fromEmail, emailPassword);
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                }
            }

        }

        public class CbmPicUpload
        {
            static string pathS3_test = @"\\52.15.86.18\storage-for-station\forTest2\";
            static string pathCsv_test = @"E:\Local\DaYuan\";
            static string pathImage_test = @"E:\Local\DaYuan\";
            static string pathImage2_test = @"E:\Local\DaYuan\";

            static string pathS3_dayuan = @"\\52.15.86.18\storage-for-station\DaYuanCbm\";
            static string pathCsv_dayuan = @"C:\條碼材積讀取程式\Log\";
            static string pathImage_dayuan = @"C:\Images\";
            static string pathImage2_dayuan = @"C:\Images2\";

            static string pathS3_taichung = @"\\52.15.86.18\storage-for-station\TaiChungCbm\";
            static string pathCsv_taichung = @"C:\條碼材積讀取程式\Log\";
            static string pathImage_taichung = @"C:\Images\";

            public static void Start(int station)            //29大園.49台中,99測試
            {
                string pathS3ForCbm = "";
                string pathCsv = "";
                List<string> pathImageList = new List<string>();

                if (station == 29)
                {
                    pathS3ForCbm = pathS3_dayuan;
                    pathCsv = pathCsv_dayuan;
                    pathImageList.Add(pathImage_dayuan);
                    pathImageList.Add(pathImage2_dayuan);
                }
                else if (station == 49)
                {
                    pathS3ForCbm = pathS3_taichung;
                    pathCsv = pathCsv_taichung;
                    pathImageList.Add(pathImage_taichung);
                }
                else
                {
                    pathS3ForCbm = pathS3_test;
                    pathCsv = pathCsv_test;
                    pathImageList.Add(pathImage_test);
                    pathImageList.Add(pathImage2_test);
                }

                Primary(pathS3ForCbm, pathCsv, pathImageList);

            }

            public static void Primary(string pathS3, string pathCsv, List<string> pathImageList)            //pathCsv是本機檔案路徑，請使用"Start"方法
            {
                string pathLog = pathS3 + "log.txt";
                bool isLogExists = File.Exists(pathLog);
                if (!isLogExists)
                {
                    var newLog = File.Create(pathLog);
                    newLog.Close();
                }

                File.AppendAllText(pathLog, DateTime.Now.ToString() + "--開始檢查本機歷程檔案" + "\r\n");

                DateTime lastFinishTime = new DateTime();
                if (Array.Exists(File.ReadAllLines(pathLog), x => x.Contains("已完成今日上傳作業")))
                {
                    string[] temp = File.ReadAllLines(pathLog).Where(x => x.Contains("已完成今日上傳作業")).ToArray();
                    DateTime[] tempDate = new DateTime[temp.Length];
                    for (var i = 0; i < tempDate.Length; i++)
                    {
                        tempDate[i] = DateTime.Parse(temp[i].Split("--")[0]);
                    }
                    Array.Sort(tempDate);

                    lastFinishTime = tempDate[tempDate.Length - 1];
                }

                Console.BackgroundColor = ConsoleColor.Green;
                Console.WriteLine("已開始上傳歷程檔案...");
                Console.BackgroundColor = ConsoleColor.Black;

                int uploadFiles = UploadFileToS3(pathCsv, pathS3 + @"log\", lastFinishTime, false, pathLog, false);

                File.AppendAllText(pathLog, DateTime.Now.ToString() + "--已完成歷程檔案上傳，共計上傳" + uploadFiles + "個檔案\r\n");

                Console.BackgroundColor = ConsoleColor.Green;
                Console.WriteLine("已完成歷程檔案上傳");
                Console.BackgroundColor = ConsoleColor.Black;

                //圖檔
                for (var i = 0; i < pathImageList.Count; i++)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine("已開始上傳圖檔..." + pathImageList[i]);
                    Console.BackgroundColor = ConsoleColor.Black;

                    int uploadImages = UploadFileToS3(pathImageList[i], pathS3 + @"pictures\", lastFinishTime, true, pathLog, false);

                    File.AppendAllText(pathLog, DateTime.Now.ToString() + "--已完成圖檔上傳，共計上傳" + uploadImages + "個檔案\r\n");

                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine("已完成" + pathImageList[i] + "歷程檔案上傳");
                    Console.BackgroundColor = ConsoleColor.Black;
                }

                File.AppendAllText(pathLog, DateTime.Now.ToString() + "--已完成今日上傳作業" + "\r\n");
            }

            private static int UploadFileToS3(string pathFrom, string pathTo, DateTime lastFinishTime, bool isNeedCreateFolder, string pathLog, bool isMove)
            {
                DirectoryInfo dir = new DirectoryInfo(pathFrom);
                FileInfo[] fileInfoArray = dir.GetFiles();
                List<string> fileList = new List<string>();


                for (var i = 0; i < fileInfoArray.Length; i++)
                {
                    if (fileInfoArray[i].LastWriteTime > lastFinishTime)
                    {
                        fileList.Add(fileInfoArray[i].Name);
                    }
                }

                try
                {
                    if (isMove)
                    {
                        ThreadMove(fileList, pathFrom, pathTo, isNeedCreateFolder);
                    }
                    else
                    {
                        ThreadCopy(fileList, pathFrom, pathTo, isNeedCreateFolder);
                    }
                }
                catch (Exception e)
                {
                    File.AppendAllText(pathLog, DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " " + e.ToString());
                }

                return fileList.Count;
            }

            public static void ThreadMove(List<string> taskList, string From, string To, bool IsNeedCreateNewDirectory)
            {
                List<string> taskList0 = new List<string>();
                List<string> taskList1 = new List<string>();
                List<string> taskList2 = new List<string>();
                List<string> taskList3 = new List<string>();
                List<string> taskList4 = new List<string>();

                var task0 = Task.Factory.StartNew(() =>
                {
                });
                var task1 = Task.Factory.StartNew(() =>
                {
                });
                var task2 = Task.Factory.StartNew(() =>
                {
                });
                var task3 = Task.Factory.StartNew(() =>
                {
                });
                var task4 = Task.Factory.StartNew(() =>
                {
                });
                var task5 = Task.Factory.StartNew(() =>
                {
                });
                var task6 = Task.Factory.StartNew(() =>
                {
                });
                var task7 = Task.Factory.StartNew(() =>
                {
                });
                var task8 = Task.Factory.StartNew(() =>
                {
                });
                var task9 = Task.Factory.StartNew(() =>
                {
                });

                for (var i = 0; i < taskList.Count; i++)
                {
                    Console.WriteLine("尚餘" + (taskList.Count - i).ToString() + "個檔案待上傳，下個任務是" + taskList[i]);
                    string fileUpdateDate = "";
                    if (IsNeedCreateNewDirectory)
                    {
                        string[] tempUpdateDate = File.GetLastWriteTime(From + @"\" + taskList[i]).ToString("yyyy/MM/dd").Split("/");
                        fileUpdateDate = tempUpdateDate[0] + tempUpdateDate[1] + tempUpdateDate[2];
                        if (!Directory.Exists(To + fileUpdateDate))
                        {
                            Directory.CreateDirectory(To + fileUpdateDate);
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine("已建立資料夾" + fileUpdateDate);
                            Console.BackgroundColor = ConsoleColor.Black;
                        }
                    }
                    Task.WaitAny(task0, task1, task2, task3, task4, task5, task6, task7, task8, task9);
                    string statusString = "";
                    string nowTask = taskList[i];
                    string destination = "";
                    if (IsNeedCreateNewDirectory)
                    {
                        destination = To + fileUpdateDate + @"\" + nowTask;
                    }
                    else
                    {
                        destination = To + nowTask;
                    }
                    List<string> statusList = new List<string>();
                    statusList.Add(CheckTheStatusOfTask(task0));
                    statusList.Add(CheckTheStatusOfTask(task1));
                    statusList.Add(CheckTheStatusOfTask(task2));
                    statusList.Add(CheckTheStatusOfTask(task3));
                    statusList.Add(CheckTheStatusOfTask(task4));
                    statusList.Add(CheckTheStatusOfTask(task5));
                    statusList.Add(CheckTheStatusOfTask(task6));
                    statusList.Add(CheckTheStatusOfTask(task7));
                    statusList.Add(CheckTheStatusOfTask(task8));
                    statusList.Add(CheckTheStatusOfTask(task9));
                    for (var j = 0; j < statusList.Count; j++)
                    {
                        statusString += statusList[j];
                    }
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine(statusString);
                    Console.BackgroundColor = ConsoleColor.Black;

                    if (task0.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task0 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task0即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task0已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task0已接獲任務" + nowTask);
                        continue;
                    }
                    if (task1.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task1 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task1即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task1已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task1已接獲任務" + nowTask);
                        continue;
                    }
                    if (task2.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task2 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task2即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task2已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task2已接獲任務" + nowTask);
                        continue;
                    }
                    if (task3.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task3 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task3即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task3已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task3已接獲任務" + nowTask);
                        continue;
                    }
                    if (task4.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task4 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task4即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task4已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task4已接獲任務" + nowTask);
                        continue;
                    }
                    if (task5.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task5 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task5即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task5已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task5已接獲任務" + nowTask);
                        continue;
                    }
                    if (task6.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task6 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task6即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task6已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task6已接獲任務" + nowTask);
                        continue;
                    }
                    if (task7.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task7 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task7即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task7已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task7已接獲任務" + nowTask);
                        continue;
                    }
                    if (task8.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task8 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task8即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task8已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task8已接獲任務" + nowTask);
                        continue;
                    }
                    if (task9.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task9 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task9即將移動" + nowTask);
                            File.Move(From + nowTask, destination, true);
                            Console.WriteLine("task9已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task9已接獲任務" + nowTask);
                        continue;
                    }
                }
                Task.WaitAll(task0, task1, task2, task3, task4, task5, task6, task7, task8, task9);
                Console.WriteLine("所有執行緒完成任務");
            }

            public static void ThreadCopy(List<string> taskList, string From, string To, bool IsNeedCreateNewDirectory)
            {
                List<string> taskList0 = new List<string>();
                List<string> taskList1 = new List<string>();
                List<string> taskList2 = new List<string>();
                List<string> taskList3 = new List<string>();
                List<string> taskList4 = new List<string>();

                var task0 = Task.Factory.StartNew(() =>
                {
                });
                var task1 = Task.Factory.StartNew(() =>
                {
                });
                var task2 = Task.Factory.StartNew(() =>
                {
                });
                var task3 = Task.Factory.StartNew(() =>
                {
                });
                var task4 = Task.Factory.StartNew(() =>
                {
                });
                var task5 = Task.Factory.StartNew(() =>
                {
                });
                var task6 = Task.Factory.StartNew(() =>
                {
                });
                var task7 = Task.Factory.StartNew(() =>
                {
                });
                var task8 = Task.Factory.StartNew(() =>
                {
                });
                var task9 = Task.Factory.StartNew(() =>
                {
                });

                for (var i = 0; i < taskList.Count; i++)
                {
                    Console.WriteLine("尚餘" + (taskList.Count - i).ToString() + "個檔案待上傳，下個任務是" + taskList[i]);
                    string fileUpdateDate = "";
                    if (IsNeedCreateNewDirectory)
                    {
                        string[] tempUpdateDate = File.GetLastWriteTime(From + @"\" + taskList[i]).ToString("yyyy/MM/dd").Split("/");
                        fileUpdateDate = tempUpdateDate[0] + tempUpdateDate[1] + tempUpdateDate[2];
                        if (!Directory.Exists(To + fileUpdateDate))
                        {
                            Directory.CreateDirectory(To + fileUpdateDate);
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine("已建立資料夾" + fileUpdateDate);
                            Console.BackgroundColor = ConsoleColor.Black;
                        }
                    }
                    Task.WaitAny(task0, task1, task2, task3, task4, task5, task6, task7, task8, task9);
                    string statusString = "";
                    string nowTask = taskList[i];
                    string destination = "";
                    if (IsNeedCreateNewDirectory)
                    {
                        destination = To + fileUpdateDate + @"\" + nowTask;
                    }
                    else
                    {
                        destination = To + nowTask;
                    }
                    List<string> statusList = new List<string>();
                    statusList.Add(CheckTheStatusOfTask(task0));
                    statusList.Add(CheckTheStatusOfTask(task1));
                    statusList.Add(CheckTheStatusOfTask(task2));
                    statusList.Add(CheckTheStatusOfTask(task3));
                    statusList.Add(CheckTheStatusOfTask(task4));
                    statusList.Add(CheckTheStatusOfTask(task5));
                    statusList.Add(CheckTheStatusOfTask(task6));
                    statusList.Add(CheckTheStatusOfTask(task7));
                    statusList.Add(CheckTheStatusOfTask(task8));
                    statusList.Add(CheckTheStatusOfTask(task9));
                    for (var j = 0; j < statusList.Count; j++)
                    {
                        statusString += statusList[j];
                    }
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine(statusString);
                    Console.BackgroundColor = ConsoleColor.Black;

                    if (task0.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task0 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task0即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task0已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task0已接獲任務" + nowTask);
                        continue;
                    }
                    if (task1.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task1 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task1即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task1已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task1已接獲任務" + nowTask);
                        continue;
                    }
                    if (task2.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task2 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task2即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task2已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task2已接獲任務" + nowTask);
                        continue;
                    }
                    if (task3.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task3 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task3即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task3已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task3已接獲任務" + nowTask);
                        continue;
                    }
                    if (task4.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task4 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task4即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task4已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task4已接獲任務" + nowTask);
                        continue;
                    }
                    if (task5.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task5 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task5即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task5已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task5已接獲任務" + nowTask);
                        continue;
                    }
                    if (task6.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task6 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task6即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task6已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task6已接獲任務" + nowTask);
                        continue;
                    }
                    if (task7.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task7 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task7即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task7已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task7已接獲任務" + nowTask);
                        continue;
                    }
                    if (task8.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task8 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task8即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task8已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task8已接獲任務" + nowTask);
                        continue;
                    }
                    if (task9.Status.Equals(TaskStatus.RanToCompletion))
                    {
                        task9 = Task.Factory.StartNew(() =>
                        {
                            Console.WriteLine("task9即將複製" + nowTask);
                            File.Copy(From + nowTask, destination, true);
                            Console.WriteLine("task9已完成當前任務");
                        });
                        Thread.Sleep(500);
                        Console.WriteLine("task9已接獲任務" + nowTask);
                        continue;
                    }
                }
                Task.WaitAll(task0, task1, task2, task3, task4, task5, task6, task7, task8, task9);
                Console.WriteLine("所有執行緒完成任務");
            }

            public static void CbmPicUploadAllPics(int station)        //丈量機圖片，不論日期全都上傳(區間:從今日前三個月到今日前一周)
            {
                string pathS3ForCbm = "";
                List<string> pathImageList = new List<string>();

                if (station == 29)
                {
                    pathS3ForCbm = pathS3_dayuan;
                    pathImageList.Add(pathImage_dayuan);
                    pathImageList.Add(pathImage2_dayuan);
                }
                else if (station == 49)
                {
                    pathS3ForCbm = pathS3_taichung;
                    pathImageList.Add(pathImage_taichung);
                }
                else
                {
                    pathS3ForCbm = pathS3_test;
                    pathImageList.Add(pathImage_test);
                    pathImageList.Add(pathImage2_test);
                }


                for (var i = 0; i < pathImageList.Count; i++)
                {
                    DateTime lastFinishTime = DateTime.Now.AddMonths(-3);   //只傳過去三個月內的
                    while (lastFinishTime < DateTime.Now.AddDays(-7))
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine("已開始上傳圖檔..." + pathImageList[i]);
                        Console.BackgroundColor = ConsoleColor.Black;

                        int uploadImages = UploadFileToS3(pathImageList[i], pathS3 + @"pictures\", lastFinishTime, true, "", false);

                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine("已完成" + pathImageList[i] + "歷程檔案上傳");
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                }
            }

            public static string CheckTheStatusOfTask(Task t)
            {
                if (t.Status.Equals(TaskStatus.RanToCompletion))
                {
                    return "1";
                }
                else if (t.Status.Equals(TaskStatus.Running))
                {
                    return "0";
                }
                else
                {
                    return "2";
                }
            }
        }


        public class CbmCalculatorEntity
        {
            public DailyCauculator[] DailyCauculatorArray { get; set; }
            public MonthlyCalculator[] MonthlyCalculatorArray { get; set; }
        }
        public class DailyCauculator
        {
            public string Date { get; set; }
            public int SuccessTotal { get; set; }
            public int FailTotal { get; set; }
            public int Total { get; set; }
            public string Percent { get; set; }
        }
        public class MonthlyCalculator
        {
            public string Month { get; set; }
            public int SuccessTotal { get; set; }
            public int FailTotal { get; set; }
            public int Days { get; set; }
            public int Total { get; set; }
        }


        public static string pathLocalSignPhoto = @"E:\WEB\JUNFU_APP_WebService\PHOTO\";
        public static string pathS3SignPhoto = @"\\52.15.86.18\storage-for-station\SignPaperPhoto\";
        public static string dataLogNameSignPhotoSchedule = @"logSchedule.txt";
        public static string dataLastDateTimeSchedule = @"lastDateTimeSchedule.txt";
        public static string dataLogNameSignPhoto = @"log.txt";
        public static string dataLastDateTime = @"lastDateTime.txt";
        public static int folderNameLength = 5;     //檔名每5個一組，每一組是一層資料夾
        public static void SignPhotoTest()
        {
            if (!File.Exists(pathS3SignPhoto + dataLogNameSignPhotoSchedule))
            {
                var newFile = File.Create(pathS3SignPhoto + dataLogNameSignPhotoSchedule);
                newFile.Close();
            };
            if (!File.Exists(pathS3SignPhoto + dataLastDateTimeSchedule))
            {
                var newFile = File.Create(pathS3SignPhoto + dataLastDateTimeSchedule);
                newFile.Close();
                File.WriteAllText(pathS3SignPhoto + dataLastDateTimeSchedule, DateTime.MinValue.ToString());
            };

            DirectoryInfo dir = new DirectoryInfo(pathLocalSignPhoto);
            DateTime lastSuccessDateTime = DateTime.Parse(File.ReadAllLines(pathS3SignPhoto + dataLastDateTimeSchedule)[0]);       //此式為程式啟動時的上一次成功檔案日期

            if (lastSuccessDateTime == DateTime.Today.AddDays(-1))
            {
                lastSuccessDateTime = lastSuccessDateTime.AddDays(-1);
            }
            DateTime[] fileCreationDateTimeArray = dir.GetFiles("*.jpg").Select(x => DateTime.Parse(x.CreationTime.ToString("yyyy-MM-dd"))).GroupBy(x => x).Select(x => x.Key).Where(x => x > lastSuccessDateTime.AddDays(1) & x < DateTime.Today.AddDays(1)).OrderBy(x => x).ToArray();
            //上式是Local資料夾內的檔案的所有日期(不包含時分秒)

            for (var i = 0; i < fileCreationDateTimeArray.Length; i++)
            {
                FileInfo[] todayFileArray = dir.GetFiles("*.jpg").Where(x => x.CreationTime >= fileCreationDateTimeArray[i] && x.CreationTime < fileCreationDateTimeArray[i].AddDays(1)).OrderBy(f => f.CreationTime).ToArray();
                /*for (var j = 0; j < todayFileArray.Length; j++)
                {
                    int folderDeepth = (int)Math.Ceiling((decimal)todayFileArray[j].Name.Length / (decimal)folderNameLength) - 1;
                    string createFolders = @"Photo\";

                    for (var k = 0; k < folderDeepth; k++)                      //布置所需資料夾
                    {
                        createFolders += todayFileArray[j].Name.Substring(k * folderNameLength, folderNameLength) + @"\";
                    }
                    Directory.CreateDirectory(pathS3SignPhoto + createFolders);

                    
                    File.Copy(todayFileArray[j].FullName, pathS3SignPhoto + createFolders + todayFileArray[j].Name, true);

                    Console.WriteLine(DateTime.Now + "已完成" + fileCreationDateTimeArray[i].ToString("yyyyMMdd") + "-----" + (decimal)((j + 1) * 100 / todayFileArray.Length) + "%");
                }*/

                MultiThreadUploaderByPart(todayFileArray, pathS3SignPhoto + @"photo\", 10, 5);
                Console.WriteLine(DateTime.Now + "已完成" + fileCreationDateTimeArray[i].ToString("yyyyMMdd"));

                File.AppendAllText(pathS3SignPhoto + dataLogNameSignPhotoSchedule, DateTime.Now + "已寫入" + fileCreationDateTimeArray[i].ToString("yyyy-MM-dd") + ",共計" + todayFileArray.Length.ToString() + "個檔案" + "\r\n");
                File.WriteAllText(pathS3SignPhoto + dataLastDateTimeSchedule, fileCreationDateTimeArray[i].ToString());
            }

            /*Array.Sort(fileArray, delegate (FileInfo f1, FileInfo f2)             //被上面的linq "orderby取代"
            {
                return f1.CreationTime.CompareTo(f2.CreationTime);
            });*/

        }

        public static void SumOfFinished()                         //非主程式，由log統計已上傳多少張照片
        {
            string[] log = File.ReadAllLines(pathS3SignPhoto + dataLogNameSignPhotoSchedule);
            int finishedPhoto = 0;
            for (var i = 0; i < log.Length; i++)
            {
                int index = log[i].IndexOf("共計");
                int nowScan = index + 2;
                int length = 1;
                int little_result = 0;
                int temp = 0;
                for (var j = nowScan; j < log[i].Length; j++)
                {
                    if (!int.TryParse(log[i].Substring(nowScan, length), out temp))
                    {
                        length--;
                        little_result = int.Parse(log[i].Substring(nowScan, length));
                        break;
                    }
                    else
                    {
                        length++;
                    }
                }
                finishedPhoto += little_result;
            }
            Console.WriteLine(finishedPhoto);
            Console.ReadKey();
        }

        static string pathLostPhotoTxt = @"E:\Local\";
        static string nameLostPhotoFile = @"LostPhoto.txt";
        static string pathS3TaiChungPhoto = @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\";
        static string pathS3TaiChungLog = @"\\52.15.86.18\storage-for-station\TaiChungCbm\log\";
        public static void GetLostPhotoOnS3()
        {
            if (!File.Exists(pathLostPhotoTxt + nameLostPhotoFile))
            {
                var nf = File.Create(pathLostPhotoTxt + nameLostPhotoFile);
                nf.Close();
            }
            DirectoryInfo dir = new DirectoryInfo(pathS3TaiChungLog);
            FileInfo[] csvFileArray = dir.GetFiles("*.csv");
            for (var i = 0; i < csvFileArray.Length; i++)
            {
                string[] fileContent = File.ReadAllLines(csvFileArray[i].FullName);
                for (var j = 0; j < fileContent.Length; j++)
                {
                    var row = fileContent[j].Split(",");
                    if (row.Length == 9 & row[0] != "NOREAD" & row[0] != "" & row[0] != "MULTRD")
                    {
                        string pictureName = row[8];
                        if (!File.Exists(pathS3TaiChungPhoto + csvFileArray[i].Name.Substring(0, 8) + @"\" + row[8]))
                        {
                            string temp = pathS3TaiChungPhoto + csvFileArray[i].Name.Substring(0, 8) + @"\" + row[8];
                            File.AppendAllText(pathLostPhotoTxt + nameLostPhotoFile, csvFileArray[i].Name.Substring(0, 8) + @"\" + row[8] + "\r\n");
                        }
                    }
                }
            }

        }

        public static void DeletePhotoBeforeDate(DateTime date, string pathPhoto)             //自動刪除指定路徑圖片檔案
        {
            DirectoryInfo dir = new DirectoryInfo(pathPhoto);
            FileInfo[] photoArray = dir.GetFiles("*.jpg");
            var needToDelete = photoArray.Where(x => x.CreationTime < date).ToArray();
            for (var i = 0; i < needToDelete.Length; i++)
            {
                Console.WriteLine("刪除" + needToDelete[i].Name + ",建立日期:" + needToDelete[i].CreationTime);
                File.Delete(needToDelete[i].FullName);
            }
        }

        static string pathS3 = @"\\52.15.86.18\storage-for-station\";
        public static void SearchPhotoFolderFromS3ByCheckNumber(string fullDataNameForNeedToSearchCheckNumbers, string outPutFullDataName)           //給CheckNumber的檔案位置，回傳圖片所在資料夾，判斷不夠好
        {
            string[] checkNumbers = File.ReadAllLines(fullDataNameForNeedToSearchCheckNumbers);
            CbmCheckNumberPhotoEntity[] resultList = new CbmCheckNumberPhotoEntity[checkNumbers.Length];
            for (var i = 0; i < checkNumbers.Length; i++)
            {
                CbmCheckNumberPhotoEntity entity = new CbmCheckNumberPhotoEntity()
                {
                    CheckNumber = checkNumbers[i]
                };
                resultList[i] = entity;
            }

            string[] csvArrayDaYuan = Directory.GetFiles(pathS3 + @"DaYuanCbm\" + @"log\", "*.csv");
            for (var i = 0; i < csvArrayDaYuan.Length; i++)
            {
                Console.WriteLine("大園正執行" + (decimal)i * 100 / csvArrayDaYuan.Length + "%");
                string[] column = File.ReadAllLines(csvArrayDaYuan[i]);
                for (var j = 0; j < column.Length; j++)
                {
                    string[] cell = column[j].Split(@",");
                    var checkNumberInS3Csv = cell[0];
                    for (var k = 0; k < resultList.Length; k++)
                    {
                        if (resultList[k].CheckNumber == checkNumberInS3Csv)
                        {
                            resultList[k].PhotoDaYuan = cell[8];
                        }
                    }
                }
            }

            string[] csvArrayTaiChung = Directory.GetFiles(pathS3 + @"TaiChungCbm\" + @"log\", "*.csv");
            for (var i = 0; i < csvArrayTaiChung.Length; i++)
            {
                Console.WriteLine("台中正執行" + (decimal)i * 100 / csvArrayTaiChung.Length + "%");
                string[] column = File.ReadAllLines(csvArrayTaiChung[i]);
                for (var j = 0; j < column.Length; j++)
                {
                    string[] cell = column[j].Split(@",");
                    var checkNumberInS3Csv = cell[0];
                    for (var k = 0; k < resultList.Length; k++)
                    {
                        if (resultList[k].CheckNumber == checkNumberInS3Csv)
                        {
                            resultList[k].PhotoTaiChung = cell[8];
                        }
                    }
                }
            }

            if (!File.Exists(outPutFullDataName))
            {
                var newFile = File.Create(outPutFullDataName);
                newFile.Close();
            }

            for (var i = 0; i < resultList.Length; i++)
            {
                File.AppendAllText(outPutFullDataName, resultList[i].CheckNumber + "," + resultList[i].PhotoDaYuan + "," + resultList[i].PhotoTaiChung + "\r\n");
            }
        }

        private static int folderNameSplitLength = folderNameLength;
        private static string logName = "logForTaiChungUploaded.txt";           //存放已經上船的資料夾
        private static string pathTaiChungSignPhotoLocal = @"D:\ScanDoc\";                    //@"D:\ScanDoc\";
        private static string pathTaiChungSignPhotoS3Txt = @"\\52.15.86.18\storage-for-station\SignPaperPhoto\";                      //pathS3SignPhoto;
        private static string pathTaiChungSignPhotoS3Photo = @"\\52.15.86.18\storage-for-station\SignPaperPhoto\photo\";
        public static void TaiChungPhotoUpload()
        {
            if (!File.Exists(pathTaiChungSignPhotoS3Txt + logName))
            {
                var f = File.Create(pathTaiChungSignPhotoS3Txt + logName);
                f.Close();
            }

            Console.WriteLine("已確認log檔案");

            string[] dirsAll = Directory.GetDirectories(pathTaiChungSignPhotoLocal);
            string[] dirsUploaded = File.ReadAllLines(pathTaiChungSignPhotoS3Txt + logName);
            string[] dirs = dirsAll.Except(dirsUploaded).ToArray();

            for (var i = 0; i < dirs.Length; i++)
            {
                DirectoryInfo dir = new DirectoryInfo(dirs[i]);
                FileInfo[] fileArray = dir.GetFiles("*.jpg");
                /*for (var j = 0; j < fileArray.Length; j++)
                {
                    int fileNameLength = fileArray[j].Name.Length;
                    int folderDeepth = (fileNameLength - 1) / folderNameLength;         //減一是為了讓整除時候少一層資料夾
                    string createFolder = pathTaiChungSignPhotoS3Photo;
                    for (var k = 0; k < folderDeepth; k++)
                    {
                        createFolder += fileArray[j].Name.Substring(k * folderSplitor, folderSplitor) + @"\";
                    }

                    Directory.CreateDirectory(createFolder);
                    Console.WriteLine(j * 100 / fileArray.Length + "%");
                    File.Copy(fileArray[j].FullName, createFolder + fileArray[j].Name, true);
                    Console.WriteLine(DateTime.Now + "正在上傳" + fileArray[j].FullName);
                }*/
                Console.WriteLine("正要執行資料夾" + dirs[i]);
                MultiThreadUploaderByPart(fileArray, pathTaiChungSignPhotoS3Photo, 10, 5);
                Console.WriteLine(DateTime.Now + "--" + dirs[i] + "傳送完畢");

                File.AppendAllText(pathTaiChungSignPhotoS3Txt + logName, dirs[i] + "\r\n");
            }
        }


        public static void CatchPhotoPathOnS3AndWriteInDBMain()
        {
            CatchPhotoPathOnS3AndWriteInDB(29);
            CatchPhotoPathOnS3AndWriteInDB(49);
        }

        public static void CatchPhotoPathOnS3AndWriteInDB(int station)
        {
            string logPathS3DaYuan = @"E:\Local\S3PhotoDaYuan.txt";
            string logPathS3TaiChung = @"E:\Local\S3PhotoTaiChung.txt";
            string photoS3Path = "";
            string DbColumn = "";
            string logPathS3 = "";
            if (station == 29)
            {
                photoS3Path = @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\";
                DbColumn = "catch_cbm_pic_path_from_S3";
                logPathS3 = logPathS3DaYuan;
            }
            else if (station == 49)
            {
                photoS3Path = @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\";
                DbColumn = "catch_taichung_cbm_pic_path_from_S3";
                logPathS3 = logPathS3TaiChung;
            }

            if (!File.Exists(logPathS3))
            {
                var newFile = File.Create(logPathS3);
                newFile.Close();
            }

            string[] foldersArray = Directory.GetDirectories(photoS3Path);
            string[] uploadedArray = File.ReadAllLines(logPathS3);
            string[] newfoldersArray = foldersArray.Except(uploadedArray).ToArray();
            for (var i = 0; i < newfoldersArray.Length; i++)
            {
                DirectoryInfo dir = new DirectoryInfo(newfoldersArray[i]);
                FileInfo[] filesArray = dir.GetFiles("*08.jpg");
                for (var j = 0; j < filesArray.Length; j++)
                {
                    if (filesArray[j].CreationTime > new DateTime(2020, 06, 19))
                    {
                        string checkNumber = filesArray[j].Name.Split("-")[0];
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandText = string.Format(@"update tcDeliveryRequests set {0} = '{1}' where check_number = '{2}'", DbColumn, filesArray[j].FullName.Replace(@"\\52.15.86.18\storage-for-station\", ""), checkNumber);
                            int feedBack;
                            using (cmd.Connection = new SqlConnection(@"Initial Catalog=JunfuReal;server=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433; Initial Catalog = JunFuReal; Persist Security Info = True; User ID = admin; Password =%Tgb^Yhn"))
                            {
                                cmd.Connection.Open();
                                feedBack = cmd.ExecuteNonQuery();
                                cmd.Connection.Close();
                            }
                            Console.WriteLine(checkNumber + " " + feedBack + "---本日" + j + "/" + filesArray.Length + "尚餘" + (newfoldersArray.Length - i) + "日--" + station);
                        }
                    }
                }

                File.AppendAllText(logPathS3, photoS3Path + newfoldersArray[i].Split(@"\")[newfoldersArray[i].Split(@"\").Length - 1] + "\r\n");
            }
        }

        public static void ForTest(string from, string to, int threadNum)
        {
            DirectoryInfo dir = new DirectoryInfo(from);
            FileInfo[] info = dir.GetFiles();
            MultiThreadUploaderByPart(info, to, threadNum, 5);
        }

        /*private static void MultiThreadUploader(FileInfo[] fileInfoArray, string fileTo, int threadNum, int folderSplitorByName)            //多執行緒上傳法,folderSplitorByName參數是依照檔名切割新增資料夾
        {
            Task[] taskArray = new Task[threadNum];
            TaskEntity[] taskEntities = new TaskEntity[threadNum];
            for (var i = 0; i < taskArray.Length; i++)
            {
                taskArray[i] = Task.Factory.StartNew(() => { });
                taskEntities[i] = new TaskEntity();
            }

            for (var i = 0; i < fileInfoArray.Length; i++)
            {
                int idleTask = Task.WaitAny(taskArray);

                taskArray[idleTask] = Task.Factory.StartNew(() =>
                {
                    int nowFileIndex = i;
                    Console.WriteLine("執行緒" + idleTask + "即將複製第" + nowFileIndex + "個檔案" + fileInfoArray[nowFileIndex].Name);
                    int nameLength = fileInfoArray[nowFileIndex].Name.Replace(".jpg", "").Length;
                    int folderDeepth = (nameLength - 1) / folderSplitorByName;
                    string fileToPath = fileTo;

                    for (var j = 0; j < folderDeepth; j++)
                    {
                        fileToPath += fileInfoArray[nowFileIndex].Name.Replace(".jpg", "").Substring(j * folderSplitorByName, folderSplitorByName) + @"\";
                    }

                    if (!Directory.Exists(fileToPath))
                    {
                        Directory.CreateDirectory(fileToPath);
                    }

                    File.Copy(fileInfoArray[nowFileIndex].FullName, fileToPath + fileInfoArray[nowFileIndex].Name, true);
                    Console.WriteLine(DateTime.Now + "已完成上傳" + fileInfoArray[nowFileIndex]);
                    //Thread.Sleep(100);
                });
                Thread.Sleep(100);
            }
        }*/

        static string uploadFileLogPath = @"uploadFileLogPath\";
        private static void MultiThreadUploaderByPart(FileInfo[] fileInfoArray, string fileTo, int threadNum, int folderSplitorByName)            //多執行緒上傳法,跟上面的差別是，這個先將任務切開,folderSplitorByName參數是依照檔名切割新增資料夾
        {
            Task[] taskArray = new Task[threadNum];
            TaskEntity[] taskEntities = new TaskEntity[threadNum];
            List<List<FileInfo>> partitionFileInfo = new List<List<FileInfo>>();
            int counter = 0;

            for (var i = 0; i < taskArray.Length; i++)
            {
                taskArray[i] = Task.Factory.StartNew(() => { });
                taskEntities[i] = new TaskEntity();
                List<FileInfo> fileInfoList = new List<FileInfo>();
                partitionFileInfo.Add(fileInfoList);
            }

            for (var i = 0; i < fileInfoArray.Length; i++)
            {
                partitionFileInfo[counter].Add(fileInfoArray[i]);
                if (counter < taskArray.Length - 1)
                {
                    counter++;
                }
                else
                {
                    counter = 0;
                }
            }

            string logFolder = pathTaiChungSignPhotoS3Txt + uploadFileLogPath + DateTime.Now.ToString("yyyyMMdd") + @"\";
            if (!Directory.Exists(pathTaiChungSignPhotoS3Txt + uploadFileLogPath))
            {
                Directory.CreateDirectory(logFolder);
            }

            counter = 0;

            for (var i = 0; i < partitionFileInfo.Count; i++)
            {
                Console.WriteLine("執行緒" + i + "獲得" + partitionFileInfo[i].Count + "個任務");
            }

            for (var i = 0; i < taskArray.Length; i++)                //不知道為啥要減一才會正常執行，後來不減
            {
                int id = i;
                try
                {
                    taskArray[id] = Task.Factory.StartNew(() =>
                    {
                        List<FileInfo> thisPartitionFile = partitionFileInfo[id];
                        for (var j = 0; j < thisPartitionFile.Count; j++)
                        {
                            Console.WriteLine("執行緒" + id + "即將複製第" + j + "個檔案" + thisPartitionFile[j].Name);
                            int nameLength = thisPartitionFile[j].Name.Replace(".jpg", "").Length;
                            int folderDeepth = (nameLength - 1) / folderSplitorByName;
                            string fileToPath = fileTo;

                            for (var k = 0; k < folderDeepth; k++)
                            {
                                fileToPath += thisPartitionFile[j].Name.Replace(".jpg", "").Substring(k * folderSplitorByName, folderSplitorByName) + @"\";
                            }

                            if (!Directory.Exists(fileToPath))
                            {
                                Directory.CreateDirectory(fileToPath);
                            }

                            try
                            {
                                File.Copy(thisPartitionFile[j].FullName, fileToPath + thisPartitionFile[j].Name, true);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.ToString());
                            }

                            Console.WriteLine(DateTime.Now + "已完成上傳" + "\r\n" + fileToPath + thisPartitionFile[j].Name + "--剩餘" + (thisPartitionFile.Count - j) + "個項目" + "\r\n");
                        }
                    });
                    Thread.Sleep(500);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            Task.WaitAll(taskArray);
        }

        public static int GetDataNum(string path)//計算指定資料夾下所有檔案總量
        {
            List<string> dirPath = new List<string>();
            string[] dir = Directory.GetDirectories(path);
            int result = 0;
            for (var i = 0; i < dir.Length; i++)
            {
                dirPath.Add(dir[i]);
            }
            for (int i = 0; i < dirPath.Count; i++)
            {
                string[] sub_dir = GetSubDirectories(dirPath[i]);
                for (var j = 0; j < sub_dir.Length; j++)
                {
                    dirPath.Add(sub_dir[j]);
                }
            }
            for (var i = 0; i < dirPath.Count; i++)
            {
                result += Directory.GetFiles(dirPath[i]).Length;
            }
            return result;
        }

        private static string[] GetSubDirectories(string path)
        {
            return Directory.GetDirectories(path);
        }

        private class CbmCheckNumberPhotoEntity
        {
            public string CheckNumber { get; set; }
            public string PhotoDaYuan { get; set; }
            public string PhotoTaiChung { get; set; }
        }

        private class TaskEntity          //對應TaskList使用
        {
            public string From { get; set; }
            public int Index { get; set; }      //
        }

        static string pathExcel = @"E:\Excel\20201130.xlsx";
        static int startIndex = 3;
        static int endIndex = 422;
        public static void InsertIntoCustomerShiperFee()
        {
            using (XLWorkbook wb = new XLWorkbook(pathExcel))
            {
                IXLWorksheet ws = wb.Worksheet("客戶資料總表");
                List<ExcelForFreight> effArray = new List<ExcelForFreight>();
                for (var i = startIndex; i < endIndex + 1; i++)
                {
                    var s = ws.Cell(4, 21).Value;
                    ExcelForFreight eff = new ExcelForFreight()
                    {
                        CustomerCode = ws.Cell(i, 1).Value.ToString().Replace("-", "").Replace("沒有客代", ""),
                        StationName = ws.Cell(i, 2).Value.ToString(),
                        CustomerName = ws.Cell(i, 3).Value.ToString(),
                        No1Bag = ws.Cell(i, 10).Value.ToString() == "" ? "null" : ws.Cell(i, 10).Value.ToString(),
                        No2Bag = ws.Cell(i, 11).Value.ToString() == "" ? "null" : ws.Cell(i, 11).Value.ToString(),
                        No3Bag = ws.Cell(i, 12).Value.ToString() == "" ? "null" : ws.Cell(i, 12).Value.ToString(),
                        No4Bag = ws.Cell(i, 13).Value.ToString() == "" ? "null" : ws.Cell(i, 13).Value.ToString(),
                        s60Cm = ws.Cell(i, 14).Value.ToString() == "" ? "null" : ws.Cell(i, 14).Value.ToString(),
                        s90Cm = ws.Cell(i, 15).Value.ToString() == "" ? "null" : ws.Cell(i, 15).Value.ToString(),
                        s110Cm = ws.Cell(i, 16).Value.ToString() == "" ? "null" : ws.Cell(i, 16).Value.ToString(),
                        s120Cm = ws.Cell(i, 17).Value.ToString() == "" ? "null" : ws.Cell(i, 17).Value.ToString(),
                        s150Cm = ws.Cell(i, 18).Value.ToString() == "" ? "null" : ws.Cell(i, 18).Value.ToString(),
                        s180Cm = ws.Cell(i, 18).Value.ToString() == "" ? "null" : ws.Cell(i, 18).Value.ToString(),
                        Holiday = ws.Cell(i, 19).Value.ToString() == "" ? "null" : ws.Cell(i, 19).Value.ToString(),
                        //PieceRate = ws.Cell(i, 15).Value.ToString() == "" ? "null" : ws.Cell(i, 15).Value.ToString()        //這行是錯的，但是我懶得改
                    };
                    string cmdText = string.Format("insert into CustomerShippingFee values('{0}','{1}','{2}',{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14})", eff.CustomerCode, eff.StationName, eff.CustomerName, eff.No1Bag, eff.No2Bag, eff.No3Bag, eff.No4Bag, eff.s60Cm, eff.s90Cm, eff.s110Cm, eff.s120Cm, eff.s150Cm, eff.s180Cm, eff.Holiday, "null");
                    using (SqlCommand cmd = new SqlCommand(cmdText))
                    {
                        cmd.Connection = new SqlConnection(@"Initial Catalog=JunfuReal;server=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433; Initial Catalog = JunFuReal; Persist Security Info = True; User ID = admin; Password =%Tgb^Yhn");
                        cmd.Connection.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch
                        {
                            string err = cmdText;
                        }
                        cmd.Connection.Close();
                    }
                    Console.WriteLine("已完成" + (i - startIndex + 1) + @"/" + (endIndex - startIndex + 1));
                    //effArray.Add(eff);
                }
            }
        }

        public static void DeleteDataAfterCheckedAndLeftLog(int station)           //確認資料在S3後，刪除該檔案，並且寫入Log，29:大園，49:台中，00:測試
        {
            int counter = 0;
            int counterNotOnS3 = 0;
            string pathC = "";      //待刪檔案路徑
            string pathS3ForCheck = "";         //S3檔案路徑
            string pathLog = "";        //log檔案路徑
            string nameLog = "";        //log檔案名稱
            string nameLogNotOnS3 = ""; //S3上沒有所以不上傳的

            if (station == 29)
            {
                pathC = @"C:\Images\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\";
                pathLog = @"\\52.15.86.18\storage-for-station\DaYuanCbm\logForDeleteDaYuanLocal\";
                nameLog = @"deleteLog.txt";
                nameLogNotOnS3 = @"noDeleteLog.txt";
            }
            else if (station == 49)
            {
                pathC = @"C:\Images\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\";
                pathLog = @"\\52.15.86.18\storage-for-station\TaiChungCbm\logForDeleteTaiChungLocal\";
                nameLog = @"deleteLog.txt";
                nameLogNotOnS3 = @"noDeleteLog.txt";
            }
            else if (station == 0)
            {
                pathC = @"E:\Local\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\";        //注意:測試路徑為台中倉資料夾
                pathLog = @"\\52.15.86.18\storage-for-station\TaiChungCbm\logForDeleteTaiChungLocal\"; //注意:測試路徑為台中倉資料夾
                nameLog = @"deleteLogForTest.txt";
                nameLogNotOnS3 = @"noDeleteLogForTest.txt";
            }
            else if (station == 292)        //大園images2
            {
                pathC = @"C:\Images2\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\";
                pathLog = @"\\52.15.86.18\storage-for-station\DaYuanCbm\logForDeleteDaYuanLocal\";
                nameLog = @"deleteLog.txt";
                nameLogNotOnS3 = @"noDeleteLog.txt";
            }
            Console.WriteLine(DateTime.Now.ToString() + "即將開始獲取本機資料");
            DirectoryInfo dirsLocal = new DirectoryInfo(pathC);
            Console.WriteLine(DateTime.Now.ToString() + "已獲取本機資料");
            FileInfo[] files = dirsLocal.GetFiles();
            DateTime[] dt = files.Select(f => f.CreationTime.Date).Distinct().Where(f => f != DateTime.Now.Date && f != DateTime.Now.AddDays(-1).Date).OrderBy(x => x).ToArray();     //避免與上傳衝突，排除兩天

            for (int i = 0; i < dt.Length; i++)
            {
                var f = files.Where(x => x.CreationTime >= dt[i] && x.CreationTime < dt[i].AddDays(1));
                if (!File.Exists(pathLog))
                {
                    var file = File.Create(pathLog + nameLog);
                    file.Close();
                }
                DirectoryInfo dirsS3 = new DirectoryInfo(pathS3ForCheck + dt[i].ToString("yyyyMMdd") + @"\");
                FileInfo[] filesS3 = dirsS3.GetFiles();

                File.AppendAllText(pathLog + nameLog, DateTime.Now.ToString("yyyyMMdd") + "開始刪除檔案" + "\r\n");

                foreach (var j in f)
                {
                    //var c = filesS3.Where(x => x.Name == j.Name).Count();
                    if (filesS3.Where(x => x.Name == j.Name).Count() > 0)
                    {
                        File.Delete(j.FullName);
                        counter++;
                        Console.WriteLine(DateTime.Now.ToString("yyyyMMdd") + "已刪除" + counter + "筆，不刪除" + counterNotOnS3 + "筆，此日共計" + f.Count() + "筆，此日為" + dt[i].ToString("yyyyMMdd"));
                        File.AppendAllText(pathLog + nameLog, "刪除時間:" + DateTime.Now.ToString() + ";檔案名稱:" + j.FullName + ";建立日期:" + j.CreationTime + "\r\n");
                    }
                    else
                    {
                        counterNotOnS3++;
                        Console.WriteLine(DateTime.Now.ToString("yyyyMMdd") + "已刪除" + counter + "筆，不刪除" + counterNotOnS3 + "筆，此日共計" + f.Count() + "筆，此日為" + dt[i].ToString("yyyyMMdd"));
                        File.AppendAllText(pathLog + nameLogNotOnS3, "刪除時間:" + DateTime.Now.ToString() + ";檔案名稱:" + j.FullName + ";建立日期:" + j.CreationTime + "\r\n");
                        File.Copy(j.FullName, j.FullName.Replace(pathC, pathS3ForCheck + dt[i].ToString("yyyyMMdd") + @"\"), true);
                        File.Delete(j.FullName);
                        Console.WriteLine("*補+" + DateTime.Now.ToString("yyyyMMdd") + "已刪除" + counter + "筆，不刪除" + counterNotOnS3 + "筆，此日共計" + f.Count() + "筆，此日為" + dt[i].ToString("yyyyMMdd"));
                        File.AppendAllText(pathLog + nameLog, "刪除時間:" + DateTime.Now.ToString() + ";檔案名稱:" + j.FullName + ";建立日期:" + j.CreationTime + "\r\n");
                    }
                }
            }
        }

        public static void SortOutByCreationTime(int station)      //將檔案依照生成日期加入資料夾
        {
            string pathC = "";
            string pathS3ForCheck = "";

            if (station == 29)
            {
                pathC = @"C:\Images\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\";
            }
            else if (station == 49)
            {
                pathC = @"C:\Images\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\";
            }
            else if (station == 0)
            {
                pathC = @"E:\Local\";
            }
            else if (station == 292)        //大園images2
            {
                pathC = @"C:\Images2\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\";
            }

            DirectoryInfo dirs = new DirectoryInfo(pathC);
            FileInfo[] file = dirs.GetFiles("*.jpg").Where(x => x.CreationTime < DateTime.Now.AddDays(-3)).ToArray();
            for (var i = 0; i < file.Length; i++)
            {
                if (!Directory.Exists(pathC + file[i].CreationTime.ToString("yyyyMMdd")))
                {
                    Directory.CreateDirectory(pathC + file[i].CreationTime.ToString("yyyyMMdd"));
                }
                File.Move(file[i].FullName, file[i].DirectoryName + @"\" + file[i].CreationTime.ToString("yyyyMMdd") + @"\" + file[i].Name);
                Console.WriteLine(DateTime.Now.ToString("yyyyMMdd-HHmmss") + "  " + file[i].Name + "  " + i + @"/" + file.Length);
            }
        }

        public static void DeleteDataAfterCheckedAndLeftLogReverse(int station)           //從S3抓回資料序列，刪除檔案，並且寫入Log，29:大園，49:台中，00:測試
        {
            int counter = 0;
            int counterNotOnS3 = 0;
            string pathC = "";      //待刪檔案路徑
            string pathS3ForCheck = "";         //S3檔案路徑
            string pathLog = "";        //log檔案路徑
            string nameLog = "";        //log檔案名稱
            string nameLogNotOnS3 = ""; //S3上沒有所以不上傳的

            if (station == 29)
            {
                pathC = @"C:\Images\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\";
                pathLog = @"\\52.15.86.18\storage-for-station\DaYuanCbm\logForDeleteDaYuanLocal\";
                nameLog = @"deleteLog.txt";
                nameLogNotOnS3 = @"noDeleteLog.txt";
            }
            else if (station == 49)
            {
                pathC = @"C:\Images\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\";
                pathLog = @"\\52.15.86.18\storage-for-station\TaiChungCbm\logForDeleteTaiChungLocal\";
                nameLog = @"deleteLog.txt";
                nameLogNotOnS3 = @"noDeleteLog.txt";
            }
            else if (station == 0)
            {
                pathC = @"E:\Local\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\";        //注意:測試路徑為台中倉資料夾
                pathLog = @"\\52.15.86.18\storage-for-station\TaiChungCbm\logForDeleteTaiChungLocal\"; //注意:測試路徑為台中倉資料夾
                nameLog = @"deleteLogForTest.txt";
                nameLogNotOnS3 = @"noDeleteLogForTest.txt";
            }
            else if (station == 292)        //大園倉images2
            {
                pathC = @"C:\Images2\";
                pathS3ForCheck = @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\";
                pathLog = @"\\52.15.86.18\storage-for-station\DaYuanCbm\logForDeleteDaYuanLocal\";
                nameLog = @"deleteLog.txt";
                nameLogNotOnS3 = @"noDeleteLog.txt";
            }

            Console.WriteLine(DateTime.Now.ToString() + "即將開始獲取本機資料");
            DirectoryInfo dirsLocal = new DirectoryInfo(pathC);
            Console.WriteLine(DateTime.Now.ToString() + "已獲取本機資料");
            FileInfo[] files = dirsLocal.GetFiles();
            string theOldestTime = files.OrderBy(x => x.CreationTime).ToArray()[0].CreationTime.ToString("yyyyMMdd");

            if (theOldestTime == DateTime.Now.ToString("yyyyMMdd") || theOldestTime == DateTime.Now.AddDays(-1).ToString("yyyyMMdd"))
            {
                return;
            }
            DirectoryInfo dirsS3 = new DirectoryInfo(pathS3ForCheck + @"\");

            DirectoryInfo[] foldersS3 = dirsS3.GetDirectories().Where(x => string.Compare(x.Name, theOldestTime) == 1).ToArray();
            for (int f = 0; f < foldersS3.Length; f++)
            {
                Console.WriteLine("正在進行" + foldersS3[f].FullName + @"____" + foldersS3[f].CreationTime);
                FileInfo[] dirPicFilesS3 = foldersS3[f].GetFiles();
                for (int i = 0; i < dirPicFilesS3.Length; i++)
                {
                    string toDeleteFile = pathC + dirPicFilesS3[i].Name;
                    if (File.Exists(toDeleteFile))
                    {
                        Console.WriteLine(DateTime.Now + "即將刪除檔案" + toDeleteFile);
                        File.Delete(toDeleteFile);
                        counter++;
                        Console.WriteLine(DateTime.Now.ToString("yyyyMMdd") + "已刪除" + counter + "筆");
                        File.AppendAllText(pathLog + nameLog, "刪除時間:" + DateTime.Now.ToString() + ";檔案名稱:" + dirPicFilesS3[i].FullName + ";建立日期:" + dirPicFilesS3[i].CreationTime + "\r\n");
                    }
                }
                Console.WriteLine("即將移動日期:" + foldersS3[f].Name);
                if (!Directory.Exists(pathC + foldersS3[f].Name))
                {
                    Directory.CreateDirectory(pathC + foldersS3[f].Name);
                }
                DateTime moveData = new DateTime(int.Parse(foldersS3[f].Name.Substring(0, 4)), int.Parse(foldersS3[f].Name.Substring(4, 2)), int.Parse(foldersS3[f].Name.Substring(6, 2)));
                DirectoryInfo dirLocal = new DirectoryInfo(pathC);
                FileInfo[] toMoveFiles = dirLocal.GetFiles().Where(x => x.CreationTime >= moveData & x.CreationTime < moveData.AddDays(1)).ToArray();

                if (toMoveFiles.Length > 0 && !Directory.Exists(pathC + foldersS3[f] + @"\"))
                {
                    Directory.CreateDirectory(pathC + foldersS3[f] + @"\");
                }
                for (var i = 0; i < toMoveFiles.Length; i++)
                {
                    Console.WriteLine("即將移動: " + toMoveFiles[i].FullName);
                    File.Move(toMoveFiles[i].FullName, pathC + foldersS3[f] + @"\" + toMoveFiles[i].Name);
                }
            }
            Console.ReadKey();
        }

        public static string folderSnapShot = @"folderSnapShot\";
        public static string folderSnapShotErr = @"folderSnapShot\err\";
        public static string divider = "---------------------------------------------------------------------------------------";      //分隔檔案與資料夾
        public static string partition = "|||||"; //屬性分隔
        public static string endNotation = "===end===";    //檔案完結
        public static void SketchFolderViscera(string motherFolderPath, string mainFolderName)        //紀錄特定資料夾內的所有jpg檔案資料
        {
            List<DirectoryInfo> foldersString = new List<DirectoryInfo>();
            DateTime startTime = DateTime.Now;
            foldersString.Add(new DirectoryInfo(motherFolderPath + mainFolderName));

            DirectoryInfo todayFolder = Directory.CreateDirectory(pathS3SignPhoto + folderSnapShot + startTime.ToString("yyyyMMdd"));
            string errLogPath = pathS3SignPhoto + folderSnapShotErr;
            Directory.CreateDirectory(errLogPath);
            string logFileFullName = todayFolder.FullName + @"\" + startTime.ToString("yyyyMMddHHmmss") + ".txt";
            var file = File.Create(logFileFullName);
            file.Close();

            try
            {
                for (var f = 0; f < foldersString.Count(); f++)
                {
                    DirectoryInfo[] subDirs = foldersString[f].GetDirectories().OrderBy(o => o.Name).ToArray();
                    for (var s = 0; s < subDirs.Length; s++)
                    {
                        foldersString.Add(subDirs[s]);
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(errLogPath + startTime.ToString("yyyyMMddHHmmss") + ".txt", DateTime.Now.ToString("yyyyMMddHHmmss") + "___" + ex.ToString() + "\r\n");
                Console.WriteLine("已完成紀錄資料夾");
            }

            File.AppendAllLines(logFileFullName, foldersString.Select(f => f.FullName + @"\"));
            File.AppendAllText(logFileFullName, divider + "\r\n");

            //以上處理完資料夾紀錄
            try
            {
                for (var f = 0; f < foldersString.Count(); f++)
                {
                    FileInfo[] files = foldersString[f].GetFiles("*.jpg");
                    File.AppendAllLines(logFileFullName, files.Select(s => s.FullName + partition + s.DirectoryName + @"\" + partition + s.CreationTime.ToString("yyyy-MM-dd HH:mm:ss") + partition + s.LastWriteTime.ToString("yyyy-MM-dd HH:mm:ss") + partition + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                    Console.WriteLine("已記錄完成資料夾內檔案" + foldersString[f].FullName + "尚餘" + (foldersString.Count() - f) + "個資料夾");
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(errLogPath + startTime.ToString("yyyyMMddHHmmss") + ".txt", DateTime.Now.ToString("yyyyMMddHHmmss") + "___" + ex.ToString());
            }
            //以上完成檔案紀錄

            File.AppendAllText(logFileFullName, endNotation);

        }


        [DllImport("Kernel32.dll", EntryPoint = "Beep")]
        public static extern bool Beep(
  int dwFreq,      // sound frequency
  int dwDuration   // sound duration
);
        public static string logSignPaperPhotoTableName = @"CheckPhotoToS3File";
        public static void FromFolderSnapShotAndWriteInDb()     //***未完成
        {
            int standard = 22;//只抓取幾點過後的的檔案，避免抓錯
            DirectoryInfo dir = new DirectoryInfo(pathS3SignPhoto + folderSnapShot);
            DateTime dtTest;
            DirectoryInfo[] dirs = dir.GetDirectories().Where(x => x.Name.Length == 8).Where(x => DateTime.TryParse(x.Name.Insert(6, "-").Insert(4, "-"), out dtTest)).OrderByDescending(x => DateTime.Parse(x.Name.Insert(6, "-").Insert(4, "-"))).ToArray();
            string[] firstNewFile = File.ReadAllLines(dirs[1].GetFiles("*.txt").Where(x => x.CreationTime.Hour >= standard).First().FullName);
            //int firstFileDividerIndex = Array.IndexOf(firstNewFile, divider);




            if (dirs.Length > 1)
            {
                string[] secondNewFile = File.ReadAllLines(dirs[0].GetFiles("*.txt").Where(x => x.CreationTime.Hour >= standard).First().FullName);
                //int secondFileDividerIndex = Array.IndexOf(secondNewFile, divider);
                string[] firstCheckNumbers = firstNewFile.Select(x => x.Split(partition)[0].Split(@"\")[x.Split(partition)[0].Split(@"\").Length - 1].Replace(".jpg", "")).ToArray();
                string[] secondCheckNumbers = secondNewFile.Select(x => x.Split(partition)[0].Split(@"\")[x.Split(partition)[0].Split(@"\").Length - 1].Replace(".jpg", "")).ToArray();
                string[] newDataCheckNumbers = secondCheckNumbers.Except(firstCheckNumbers).ToArray();
                string[] newData = secondNewFile.Where(x => newDataCheckNumbers.Contains(x.Split(partition)[0].Split(@"\")[x.Split(partition)[0].Split(@"\").Length - 1].Replace(".jpg", ""))).ToArray();
                InsertIntoSignPaperPhoto(newData);

                //以下避免文檔有問題造成大量重複單號
                try
                {
                    string cmdText = @"delete from CheckPhotoToS3File where file_id not in (select min(file_id) from CheckPhotoToS3File with(nolock) group by file_name)";
                    SqlCommand cmd = new SqlCommand(cmdText);
                    cmd.Connection = new SqlConnection(AWSDataBaseConnectionString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                string[] newData = firstNewFile;
                InsertIntoSignPaperPhoto(newData);
            }
        }

        private static void InsertIntoSignPaperPhoto(string[] input)
        {
            using (SqlConnection conn = new SqlConnection(AWSDataBaseConnectionString))
            {
                conn.Open();
                string cmdText = string.Format(@"insert into {0} (file_name, folder_name, file_create_date, file_latest_edit_time, cdate) values ", logSignPaperPhotoTableName);
                for (var i = 0; i < input.Length; i++)
                {
                    string[] dataDetail = input[i].Split(partition);
                    if (dataDetail.Length == 5)
                    {
                        string dataShortName = dataDetail[0].Split(@"\")[dataDetail[0].Split(@"\").Length - 1].Replace(".jpg", "");
                        cmdText += string.Format(@"('{0}','{1}','{2}','{3}','{4}'),", dataShortName, dataDetail[1], dataDetail[2], dataDetail[3], dataDetail[4]);

                        if (i % 1000 == 999 || (i == input.Length - 1))
                        {
                            //Beep(1000, 1000);
                            cmdText = cmdText.Substring(0, cmdText.Length - 1);
                            SqlCommand cmd = conn.CreateCommand();
                            cmd.CommandText = cmdText;
                            cmd.ExecuteScalar();
                            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "資料庫寫入:" + dataShortName + "，已完成" + i + "筆，剩餘" + (input.Length - i) + "筆");
                            cmdText = string.Format(@"insert into {0} (file_name, folder_name, file_create_date, file_latest_edit_time, cdate) values ", logSignPaperPhotoTableName);
                        }
                    }
                }
                conn.Close();
            }
        }

        public static void InsertIntoSignPaperPhotoInfoOnS3()
        {
            string cmdUpdateNewFileText = @"update CheckPhotoToS3File set CheckPhotoToS3File.delivery_request_id = d.request_id from tcDeliveryRequests d where d.check_number = CheckPhotoToS3File.file_name and delivery_request_id is null";
            using (SqlCommand cmd = new SqlCommand(cmdUpdateNewFileText))
            {
                cmd.Connection = new SqlConnection(AWSDataBaseConnectionString);
                try
                {
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            Console.WriteLine("已更新完畢新資料");

            string cmdText = @"select file_name from CheckPhotoToS3File with(nolock) where is_uploaded is null";
            List<string> cns = new List<string>();
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSDataBaseConnectionString);
                    cmd.Connection.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (dr["file_name"].ToString().Trim(' ').Length == 12)
                            {
                                cns.Add(dr["file_name"].ToString());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            using (SqlConnection conn = new SqlConnection(AWSDataBaseConnectionString))
            {
                conn.Open();
                int cnsLength = cns.Count();
                string cmdTextUpdate = @"";
                int countText = 0;
                for (var i = 0; i < cnsLength; i++)      //***起始要記得改成0
                {
                    Console.WriteLine(DateTime.Now.ToString("yyyyMMdd HHmmss") + "已執行" + i.ToString() + "筆，總計" + cnsLength + "筆，待更新" + countText + "筆，此筆為" + cns[i]);
                    try
                    {
                        DirectoryInfo dir = new DirectoryInfo(pathTaiChungSignPhotoS3Photo + CheckNumberPath(cns[i]));
                        FileInfo[] files = dir.GetFiles(cns[i] + ".jpg");       //***這行要修

                        if (files.Length > 0)
                        {
                            cmdTextUpdate += string.Format(@"update CheckPhotoToS3File set is_uploaded = 1, upload_time = '{1}', s3_path = '{2}' where file_name = '{0}';", files[0].Name.Replace(".jpg", ""), files[0].CreationTime.ToString("yyyy-MM-dd HH:mm:ss"), files[0].FullName);
                            countText += 1;
                        }

                        if (countText == 100 || (i == (cnsLength - 1) && cmdTextUpdate.Length > 0))
                        {
                            SqlCommand cmd = conn.CreateCommand();
                            cmd.CommandText = cmdTextUpdate;
                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                            finally
                            {

                            }
                            countText = 0;
                            cmdTextUpdate = @"";
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
                conn.Close();
            }
        }

        private static string CheckNumberPath(string checkNumber)      //計算檔案位置
        {
            int folderDeepth = checkNumber.Length / 5;        //每五個字一組
            string SubPath = @"";
            for (var d = 0; d < folderDeepth; d++)
            {
                SubPath += checkNumber.Substring(5 * d, 5) + @"\";
            }

            return SubPath;
        }

        public static void AutoCreateDirectoryOnS3()     //自動將簽單所需的所有資料夾建齊
        {
            Console.WriteLine("格式:XXXXX，五個數字即可");
            string motherFolder = Console.ReadLine() + @"\";
            Console.WriteLine("從幾號開始，五個數字即可");
            string folderStart = Console.ReadLine();
            int folderStartInt = Int32.Parse(folderStart);

            DirectoryInfo dirs = new DirectoryInfo(pathS3SignPhoto + @"photo\" + motherFolder);
            DirectoryInfo[] getAllFolders = dirs.GetDirectories();
            string checkFolderName = "";
            for (var i = folderStartInt; i < 100000; i++)
            {
                Console.WriteLine("正要確認" + i.ToString("00000"));
                checkFolderName = pathS3SignPhoto + @"photo\" + motherFolder + i.ToString("00000");
                if (getAllFolders.Where(x => x.Name == i.ToString("00000")).Count() == 0)
                {
                    Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "正要建立" + i.ToString("00000"));
                    Directory.CreateDirectory(checkFolderName);
                    Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "已建立" + i.ToString("00000"));
                }
            }
        }

        public static void CarryDataFromLocalToS3Cbm(string daYuanOrTaiChung = "", string dateTimeInString = "", string localPath = "", bool skipIfInputError = false)      //將指定日期的檔案搬上S3
        {
            while (daYuanOrTaiChung != "D" && daYuanOrTaiChung != "T")
            {
                if (daYuanOrTaiChung == string.Empty)
                {
                    Console.WriteLine("請輸入目前所在機器(D/T)");
                }
                else if (skipIfInputError)
                {
                    Console.WriteLine("給我重打!");
                }
                else
                {
                    return;
                }
                daYuanOrTaiChung = Console.ReadLine();
            }

            DateTime dateTimeInput;
            while (dateTimeInString == "" || !DateTime.TryParse(dateTimeInString, out dateTimeInput))
            {
                if (dateTimeInString == string.Empty)
                {
                    Console.WriteLine("請輸入欲傳上S3的檔案日期(Format:yyyy-MM-dd)");
                }
                else if (skipIfInputError)
                {
                    Console.WriteLine("給我重打!");
                }
                else
                {
                    return;
                }
                dateTimeInString = Console.ReadLine();
            }

            while (localPath == "" || !Directory.Exists(localPath) || !localPath.EndsWith(@"\"))
            {
                if (localPath == string.Empty)
                {
                    Console.WriteLine(@"請輸入本機路徑(C:\\....)");
                }
                else if (skipIfInputError)
                {
                    Console.WriteLine("給我重打!");
                }
                else
                {
                    return;
                }
                localPath = Console.ReadLine();
            }

            string S3Path = daYuanOrTaiChung == "D" ? @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\" : daYuanOrTaiChung == "T" ? @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\" : "";

            DirectoryInfo dirs = new DirectoryInfo(localPath);
            FileInfo[] files = dirs.GetFiles("*.jpg").Where(x => x.CreationTime > dateTimeInput && x.CreationTime < dateTimeInput.AddDays(1)).ToArray();
            Console.Write("本次合計" + files.Length + "個檔案,三秒後開始執行");
            //Thread.Sleep(3000);
            Console.Clear();
            Console.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "已啟動..");

            if (!Directory.Exists(S3Path + dateTimeInput.ToString("yyyyMMdd")))
            {
                Directory.CreateDirectory(S3Path + dateTimeInput.ToString("yyyyMMdd"));
            }

            for (int f = 0; f < files.Length; f++)
            {
                File.Copy(files[f].FullName, S3Path + files[f].CreationTime.ToString("yyyyMMdd") + @"\" + files[f].Name, true);
                File.Delete(files[f].FullName);
                Console.Clear();
                Console.WriteLine(string.Format(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 執行日期:{0} ,已上傳{1}/{2} {3}", dateTimeInString, f + 1, files.Length, f % 4 == 0 ? "|" : f % 4 == 1 ? "/" : f % 4 == 2 ? "-" : f % 4 == 3 ? @"\" : ""));
            }
        }

        public static void AutoCarryDataFromLocalToS3(string station = "", string localPath = "")     //自動代入參數至上方方法
        {
            while (station == string.Empty)
            {
                Console.WriteLine("請輸入站所(D/T)");
                station = Console.ReadLine();
            }

            while (localPath == string.Empty)
            {
                Console.WriteLine(@"請輸入本機路徑(C:\\....)");
                localPath = Console.ReadLine();
            }

            DateTime endDate = DateTime.Now.AddDays(-7);
            DateTime exeDate = endDate;
            DirectoryInfo dirs = new DirectoryInfo(localPath);
            while (dirs.GetFiles("*.jpg").Where(x => x.CreationTime < exeDate).Count() > 0)
            {
                Console.WriteLine("執行日期:" + exeDate.ToString("yyyy-MM-dd"));
                CarryDataFromLocalToS3Cbm(station, exeDate.ToString("yyyy-MM-dd"), localPath);
                exeDate = exeDate.AddDays(-1);
            }

            DirectoryInfo[] directories = dirs.GetDirectories();
            exeDate = endDate;
            while (directories.Length > 0)
            {
                Console.WriteLine("執行日期:" + exeDate.ToString("yyyy-MM-dd"));
                if (Directory.Exists(localPath + string.Format(@"{0}\", exeDate.ToString("yyyyMMdd"))))
                {
                    CarryDataFromLocalToS3Cbm(station, exeDate.ToString("yyyy-MM-dd"), localPath + string.Format(@"{0}\", exeDate.ToString("yyyyMMdd"), true));
                    DirectoryInfo subDir = new DirectoryInfo(localPath + string.Format(@"{0}\", exeDate.ToString("yyyyMMdd")));
                    if (subDir.GetFiles().Length == 0)
                    {
                        subDir.Delete();
                    }
                }
                exeDate = exeDate.AddDays(-1);
            }
        }

        private class ExcelForFreight
        {
            public string CustomerCode { get; set; }
            public string StationName { get; set; }
            public string CustomerName { get; set; }
            public string No1Bag { get; set; }
            public string No2Bag { get; set; }
            public string No3Bag { get; set; }
            public string No4Bag { get; set; }
            public string s60Cm { get; set; }
            public string s90Cm { get; set; }
            public string s110Cm { get; set; }
            public string s120Cm { get; set; }
            public string s150Cm { get; set; }
            public string s180Cm { get; set; }
            public string Holiday { get; set; }
            public string PieceRate { get; set; }
        }
    }


    public class MyWebClient : WebClient
    {
        public static void GoWebClient()            //用WebClient呼叫82上的WebService
        {
            WebRequest wrRequestDaYuan = WebRequest.Create("http://awswebservice.fs-express.com.tw/Webservice.asmx/DaYuanDataCarryer");
            wrRequestDaYuan.Timeout = 20 * 60 * 1000 * 1000;
            var responseDaYuan = wrRequestDaYuan.GetResponse();
            responseDaYuan.Close();

            WebRequest wrRequestTaiChung = WebRequest.Create("http://awswebservice.fs-express.com.tw/Webservice.asmx/TaiChungDataCarryer");
            wrRequestTaiChung.Timeout = 20 * 60 * 1000 * 1000;
            var responseTaiChung = wrRequestTaiChung.GetResponse();
            responseTaiChung.Close();
        }

    }

    public class AwsWebClient : WebClient
    {
        static string fixedTime = "093000";        //早上九點半
        public static void GoWebClient()            //用WebClient呼叫AWS上的Controller更新丈量資訊在專屬的Table
        {
            try
            {
                WebRequest wrRequestDaYuan = WebRequest.Create(string.Format(@"https://erp.fs-express.com.tw/cbmdatalog/insertintocbmdatalog?date={0}&station={1}", DateTime.Now.ToString("yyyyMMdd") + fixedTime, "29"));
                wrRequestDaYuan.Timeout = 3 * 1000 * 1000;
                var responseDaYuan = wrRequestDaYuan.GetResponse();
                responseDaYuan.Close();
            }
            catch (Exception ex)
            {

            }

            try
            {
                WebRequest wrRequestTaiChung = WebRequest.Create(string.Format(@"https://erp.fs-express.com.tw/cbmdatalog/insertintocbmdatalog?date={0}&station={1}", DateTime.Now.ToString("yyyyMMdd") + fixedTime, "49"));
                wrRequestTaiChung.Timeout = 3 * 1000 * 1000;
                var responseTaiChung = wrRequestTaiChung.GetResponse();
                responseTaiChung.Close();
            }
            catch (Exception ex)
            {

            }
        }

    }

    public class GuoYangWebClient : WebClient
    {
        public static void GoWebClient(int step)
        {
            WebRequest wrRequest = WebRequest.Create(string.Format("https://erp.fs-express.com.tw/intermodal/GuoYangSchedule?step={0}", step));
            wrRequest.Timeout = 20 * 60 * 1000 * 1000;
            var response = wrRequest.GetResponse();
            response.Close();
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            /*string sqlConnectionString105 = @"Initial Catalog=Junfu;server=172.31.1.105;User ID=sa;Password=%Tgb^Yhn";
            string sqlConnectionString105Trans = @"Initial Catalog=JunfuTrans;server=172.31.1.105;User ID=sa;Password=%Tgb^Yhn";
            string sqlConnectionString104 = @"Initial Catalog=Junfu;server=172.31.1.104;User ID=junfu_lt;Password=iVaOXJbMpC24";
            Tools.GetDBColumns(sqlConnectionString105, "tcDeliveryRequests", sqlConnectionString105Trans, "FAKE_tcDeliveryRequests");*/



            //MyWebClient.GoWebClient();
            //AwsWebClient.GoWebClient();     //自動寫入丈量資訊進專屬Table
            //Tools.MainCbm();                      //寄送丈量機不良率Excel檔
            //Tools.SignPhotoTest();                 //自動上傳簽單到S3,test
            //Tools.FromFolderSnapShotAndWriteInDb();     //自動讀取簽單快照檔案並寫入資料庫
            //Tools.InsertIntoSignPaperPhotoInfoOnS3();
            //Tools.CbmPicUpload.Start(29);    //丈量機上傳圖片(經典)
            //Tools.CbmPicUpload.CbmPicUploadAllPics(29);
            //Tools.TaiChungPhotoUpload();    //自動上傳簽單到S3  


            //~~~~~~~~~不知道哪裡有使用~~~~~~~~~~~

            //Tools.GetAllCbmLogData(); 
            //Tools.SumOfFinished();                  //由log檔取得已經上傳多少簽單圖檔  
            //Tools.GetLostPhotoOnS3();                 //找到那些丈量機圖片不在S3上   
            /*
            Console.WriteLine("請輸入欲刪除月份(不含)");              //刪丈量機圖片用
            int i = int.Parse(Console.ReadLine());
            Tools.DeletePhotoBeforeDate(new DateTime(2020, i, 01), @"C:\Images\");  
            Tools.DeletePhotoBeforeDate(new DateTime(2020, i, 01), @"C:\Images2\");
            */

            //Tools.SearchPhotoFolderFromS3ByCheckNumber(@"E:\Local\cn.txt", @"E:\Local\rcn.txt"); 
            //Tools.SketchFolderViscera(@"D:\", @"ScanDoc\"); 

            //Tools.CatchPhotoPathOnS3AndWriteInDBMain(); 

            /*
            string from = @"E:\ImS4\";            //測試用
            string to = @"E:\AnotherFakeS3\";
            Tools.ForTest(from, to, 10);
            */

            //Tools.GetDataNum(@"E:\AnotherFakeS3\");
            //Tools.InsertIntoCustomerShiperFee();      //自動輸入運價表 


            //try
            //{
            //    Tools.DeleteDataAfterCheckedAndLeftLogReverse(292);
            //}
            //catch (Exception e)
            //{
            //    if (!File.Exists(@"C:\Exceptions\ex.txt"))
            //    {
            //        var newFile = File.Create(@"C:\Exceptions\ex.txt");
            //        newFile.Close();
            //    }
            //    File.AppendAllText(@"C:\Exceptions\ex.txt", DateTime.Now.ToString("yyyyMMdd-HHmmss") + "   " + e.ToString());
            //}


            //Tools.SortOutByCreationTime(292); 
            //Tools.AutoCreateDirectoryOnS3();  
            //GuoYangWebClient.GoWebClient(2);    //參數意義請參考橘子 
            //Tools.CarryDataFromLocalToS3Cbm();
            //Tools.AutoCarryDataFromLocalToS3(); 



            //~~~~~~~~~172.31.2.82(IDC)~~~~~~~~~

            //Tools.MainCbm("大園倉", "台中倉");   //自動Email丈量機不良率 //重複之後會關掉

            //~~~~~~~~~172.31.3.80(IDC)~~~~~~~~~

            //Tools.SignPhotoTest();   //UploadSignPaperSchedule //只有S3

            //~~~~~~~~~3.142.119.175(AWS)~~~~~~~~~

            //Tools.InsertIntoSignPaperPhotoInfoOnS3();  //Tools-更新資料表CheckPhotoToS3File //只有DB(資料5/20就沒)
            Tools.MainCbm("大園倉", "台中倉");    //EmailCbmAbnormalRate  //之後記得改排程要每天都發送 //只有S3
            //Tools.FromFolderSnapShotAndWriteInDb();   //Tools-讀取簽單快照檔案並寫入資料庫 //目前都沒有資料(到5/20) 不知道哪一個對應的有問題
            //AwsWebClient.GoWebClient();          //Tools-寫入丈量機資訊到專屬資料表cbm_data_log //呼叫API(DNS erp.fs-express.com.tw)
            //MyWebClient.GoWebClient();   //WriteInDbCbm  //呼叫API(DNS awswebservice.fs-express.com.tw)

            //~~~~~~~~~台中~~~~~~~~~

            //Tools.CbmPicUpload.CbmPicUploadAllPics(49);   //thread補上傳 //圖都丟到S3
            // Tools.CbmPicUpload.Start(49);   //自動上傳S3 //把CSV跟圖都丟到S3

            //~~~~~~~~~大園~~~~~~~~~
            //Tools.CbmPicUpload.CbmPicUploadAllPics(29);   //thread補上傳  //圖都丟到S3
            //Tools.CbmPicUpload.Start(29);    //自動上傳S3  //把CSV跟圖都丟到S3

            //~~~~~~~~~4F電腦~~~~~~~~~
            // Tools.TaiChungPhotoUpload();  //把四樓指定目錄底下的圖片 排程自動輩份到S3 

            //Console.WriteLine("執行完畢");
            //Console.ReadKey();
        }
    }
}

